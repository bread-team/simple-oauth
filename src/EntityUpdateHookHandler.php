<?php

namespace Drupal\simple_oauth;

use Drupal\consumers\Entity\ConsumerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\simple_oauth\Event\UserUpdateTokenInvalidationEvent;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Service that dispatches events on specific entity updates.
 */
final class EntityUpdateHookHandler {

  /**
   * Token collector.
   *
   * @var \Drupal\simple_oauth\ExpiredCollector
   */
  protected ExpiredCollector $expiredCollector;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * EntityUpdateHookHandler constructor.
   *
   * @param \Drupal\simple_oauth\ExpiredCollector $expired_collector
   *   The expired collector service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    ExpiredCollector $expired_collector,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->expiredCollector = $expired_collector;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Handle invalidation of tokens secondary to entity update.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Updated entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function handleEntityUpdate(EntityInterface $entity): void {
    // Collect the affected tokens and expire them.
    if ($entity instanceof UserInterface) {
      // Following the Drupal core pattern, revoke access to the system when
      // password has been changed.
      $password_changed = $entity->pass->value !== $entity->original->pass->value;

      // When the account got blocked, we should revoke user's access to
      // the site.
      $account_blocked = !$entity->isActive() && $entity->original->isActive();

      // When roles have changed, we should revoke the token to prevent
      // potentia access to the content where the user has no access to anymore.
      $roles_new = $entity->getRoles();
      $roles_old = $entity->original->getRoles();
      $roles_changed = !empty(array_diff($roles_new, $roles_old)) || !empty(array_diff($roles_old, $roles_new));

      if ($password_changed || $account_blocked || $roles_changed) {
        $event = new UserUpdateTokenInvalidationEvent(
          $password_changed || $account_blocked || $roles_changed,
          $entity
        );
        $this->eventDispatcher->dispatch($event);
        if ($event->willInvalidateAccessTokens()) {
          $this->expiredCollector
            ->deleteMultipleTokens($this->expiredCollector->collectForAccount(
              $entity,
              $event->willInvalidateRefreshTokens()
            ));
        }
      }
    }
    if ($entity instanceof ConsumerInterface) {
      $this->expiredCollector
        ->deleteMultipleTokens($this->expiredCollector->collectForClient($entity));
    }
  }

  /**
   * Handle invalidation of tokens secondary to user delete.
   *
   * @param \Drupal\user\UserInterface $user
   *   Updated user.
   */
  public function handleUserDelete(UserInterface $user): void {
    $this->expiredCollector
      ->deleteMultipleTokens($this->expiredCollector->collectForAccount(
        $user,
        TRUE
      ));
  }

}
