<?php

namespace Drupal\simple_oauth\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Event for determining whether to invalidate user tokens on update.
 */
final class UserUpdateTokenInvalidationEvent extends Event {

  /**
   * The user entity.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * Whether Simple OAuth has determined that the user's access has changed.
   *
   * @var bool
   */
  protected bool $haveAccessCharacteristicsChanged;

  /**
   * Whether to invalidate user's access tokens.
   *
   * This is TRUE by default to maintain BC. To only invalidate when access
   * characteristics have changed, implement an event subscriber to set this to
   * the value of ::haveUserAccessCharacteristicsChanged().
   *
   * @var bool
   */
  protected ?bool $invalidateAccessTokens = TRUE;

  /**
   * Whether to invalidate user's refresh tokens.
   *
   * @var bool
   */
  protected ?bool $invalidateRefreshTokens = FALSE;

  /**
   * Get the changed user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity.
   */
  public function getUser(): UserInterface {
    return $this->user;
  }

  /**
   * Get the unchanged user entity.
   *
   * @return \Drupal\user\UserInterface|null
   *   The unchanged user entity.
   */
  public function getOriginalUser(): ?UserInterface {
    return $this->user->original ?? NULL;
  }

  /**
   * Setter for access token invalidation.
   *
   * @param bool $flag
   *   Whether to invalidate access tokens.
   */
  public function setInvalidateAccessTokens(bool $flag): void {
    $this->invalidateAccessTokens = $flag;
  }

  /**
   * Getter for access token invalidation behavior.
   *
   * @return bool
   *   Determination.
   */
  public function willInvalidateAccessTokens(): bool {
    return $this->invalidateAccessTokens;
  }

  /**
   * Getter for refresh token invalidation behavior.
   *
   * @return bool
   *   Determination.
   */
  public function willInvalidateRefreshTokens(): bool {
    return $this->invalidateRefreshTokens;
  }

  /**
   * Setter for refresh token invalidation.
   *
   * @param bool $flag
   *   Whether to invalidate refresh tokens.
   */
  public function setInvalidateRefreshTokens(bool $flag): void {
    $this->invalidateRefreshTokens = $flag;
    if ($flag === TRUE) {
      $this->invalidateAccessTokens = TRUE;
    }
  }

  /**
   * Getter for Simple OAuth's determination of access characteristic change.
   *
   * @return bool
   *   TRUE if the user's roles, password or status has changed.
   */
  public function haveUserAccessCharacteristicsChanged(): bool {
    return $this->haveAccessCharacteristicsChanged;
  }

  /**
   * Constructor.
   *
   * @param bool $access_characteristics_have_changed
   *   Flag for whether user access characteristics have changed.
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   */
  public function __construct(bool $access_characteristics_have_changed, UserInterface $user) {
    $this->haveAccessCharacteristicsChanged = $access_characteristics_have_changed;
    $this->user = $user;
  }

}
